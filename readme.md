# Fullstack IoT
> This codebase is part of series on teaching the technologies involved in when building an IoT SaaS products. Branches are suffixed along these four  dimensions:
> - `*-industrial-onprem`
> - `*-consumer-onprem`
> - `*-industrial-cloud`
> - `*-consumer-cloud`
>
> Where `*` represents the section. The project is divided up in sections so you can see its evolution.  Below you'll find the overall deployment diagram and well as cloud and onprem variations. As well as each sections outline.

**Note:**  Since a test broker is needed so often its probably best to have one always up and running.  The quickest way to boot one up is by typing 
`docker run -d --name emqx -p 18083:18083 -p 1883:1883 emqx:latest`. 


### Deployment Diagram
```plantuml
rectangle industrial_module #line.dashed {
	rectangle modbus_client
	rectangle dnp3_main
}
rectangle consumer_module #line.dashed {
	rectangle temp_humidity
	rectangle air_quality
}
rectangle nest_api {
	rectangle rest_api
	rectangle async_api
	rectangle biz_logic
}
rectangle web_broker
database timeseries
database document
rectangle device_broker
rectangle identity
person api_user
rectangle spa_server
modbus_client ... device_broker
dnp3_main ... device_broker
air_quality ... device_broker
temp_humidity ... device_broker
device_broker -d- biz_logic
document -r- biz_logic
timeseries -r- biz_logic
biz_logic -d- rest_api
biz_logic -d- async_api 
rest_api -d- identity
async_api -d- web_broker
web_broker -d- identity
identity -d- spa_server
identity -d- api_user
```

## Cloud Variation
- iac: aws-cdk
- container: fargate
- timeseries: timestream
- document: dynamodb
- *_broker: iot
- identity: cognito
- spa server: s3
## Onprem Variation
- iac: docker compose
- container: docker
- timeseries: timescale
- document: mongodb
- *_broker: emqx
- identity: keycloak
- spa server: s3www/minio

## Sections
### `01-scaffolding-[industrial|consumer]-[onprem|cloud]`
1. Nest API - Device Side
    - nestjs-asyncapi
2. Devices: DNP3 and Modbus
    1. mock server/outstation
    2. mock device broker
3. Nest API - Web Side
    1. mock device broker
    2. mock web broker
4. SPA:
    1. mock web broker
    2. playwright
5. REST API
    1. Timeseries DB
    2. Document DB
    3. Identity Provider
    4. SPA: Backfill and subscribe pattern line chart
6. End to End Testing
    1. Load
    2. Regression