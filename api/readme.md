# 🕸 API

```plantuml
rectangle test_device #line.dashed
rectangle nest_api {
	rectangle rest_api
    rectangle async_api
    rectangle biz_logic
}
rectangle web_broker
database timeseries
database document
rectangle device_broker
rectangle identity
person test_client #line.dashed
test_device -- device_broker
device_broker -(0)-- biz_logic
document -r- biz_logic
timeseries -r- biz_logic
biz_logic -d- rest_api
biz_logic -d- async_api 
rest_api -d- identity
async_api -d- web_broker
web_broker -d- identity
identity -(0)- test_client 
```