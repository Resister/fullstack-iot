# 🌍 Web
```plantuml
rectangle spa
rectangle test_api
rectangle test_device
rectangle playwright
rectangle test_broker #line.dashed
test_device -l- test_broker
spa -u- test_broker
spa -u- test_api
spa -d- playwright
```