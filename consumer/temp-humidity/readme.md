# 🤖 Air Quality Monitor
```plantuml
rectangle test_driver
rectangle application
rectangle test_mqtt_client
rectangle test_broker #line.dashed
test_driver -r(0)- application
application -r(0)- test_broker
test_broker -r- test_mqtt_client
```
