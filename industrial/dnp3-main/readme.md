# 🤖 DNP3 Main
## Test Fixtures
```plantuml
rectangle test_dnp3_outstation
rectangle dnp3_main
rectangle test_mqtt_client
rectangle test_broker #line.dashed
test_dnp3_outstation -r(0)- dnp3_main
dnp3_main -r(0)- test_broker
test_broker -r- test_mqtt_client
```


