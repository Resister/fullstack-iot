# 🤖 Modbus Client
## Test Fixtures
```plantuml
rectangle test_modbus_server
rectangle modbus_client
rectangle test_mqtt_client
rectangle test_broker #line.dashed
test_modbus_server -r(0)- modbus_client
modbus_client -r(0)- test_broker
test_broker -r- test_mqtt_client
```


